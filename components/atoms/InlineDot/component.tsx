import styled from '@emotion/styled';
import { Props } from './props';
import { css } from '@emotion/react';

export const InlineDot = styled.div<Props>`
  ${({ theme }) => css`
    width: 4px;
    height: 4px;
    border-radius: 5px;
    margin: 0 10px;
    background: ${theme.greyText};
    transform: translateY(1px);
  `}
`;
