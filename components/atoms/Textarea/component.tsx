import React, { FC } from 'react';
import { Props } from './props';
import { useTheme } from '@emotion/react';

export const Textarea: FC<Props> = ({ label, ...rest }: Props) => {
  const theme = useTheme();
  return (
    <label>
      {label && (
        <span css={{
          display: "block",
          fontSize: 13,
          marginBottom: 5,
          color: theme.greyText,
        }}>{label}</span>
      )}
      <textarea
        css={{
          display: "block",
          width: "100%",
          minWidth: "100%",
          minHeight: 200,
          maxHeight: 2000,
          borderRadius: 3,
          border: `2px solid ${theme.greyBorder}`,
          marginBottom: 10,
          fontSize: 13,
          padding: 10,
        }}
        {...rest}
      />
    </label>
  );
}
