import { HtmlHTMLAttributes } from 'react';

export type Props = HtmlHTMLAttributes<HTMLHeadingElement>;
