import styled from '@emotion/styled';
import { Heading } from '../Heading';
import { Props } from './props';

export const Title = styled(Heading)<Props>`
  font-size: 27px;
  font-weight: 800;
  margin-top: 0;
  margin-bottom: 10px;
`;
