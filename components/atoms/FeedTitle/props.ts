import { TitleProps } from '../Title';

export type Props = TitleProps;
