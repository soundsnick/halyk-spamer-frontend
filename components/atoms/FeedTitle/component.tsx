import { Props } from './props';
import styled from '@emotion/styled';
import { Title } from '../Title';

export const FeedTitle = styled(Title)<Props>`
  font-size: 40px;
  font-weight: 700;
  line-height: 1.05;
  margin: 0;
  margin-bottom: 25px;
`;
