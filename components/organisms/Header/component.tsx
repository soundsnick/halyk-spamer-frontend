import React, { FC, Fragment, useContext, useRef } from 'react';
import { Props } from './props';
import { Container } from '../../atoms/Container';
import { Heading } from '../../atoms/Heading';
import { AnchorButton } from '../../atoms/AnchorButton';
import { useRouter } from 'next/router';
import { NavigationItem } from './libs/NavigationItem';
import { useTheme } from '@emotion/react';
import { TokenContext, UserContext } from '../../contexts';
import { Dropdown } from '../../molecules/Dropdown';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faSearch } from '@fortawesome/free-solid-svg-icons';
import { Card } from '../../molecules/Card';
import { DropdownItem } from './libs/DropdownItem';
import { Avatar } from '../../molecules/Avatar';
import { controller, getAvatar } from '../../../core';
import { Divider } from '../../atoms/Divider';

export const Header: FC<Props> = ({ ...rest }: Props) => {
  const router = useRouter();
  const theme = useTheme();
  const token = useContext(TokenContext);

  const isAuthenticated = token && token.length > 0;
  const user = useContext(UserContext);

  const handleLogout = async () => {
    localStorage.removeItem("access_token");
    location.reload();
  }

  return (
    <header css={{ background: theme.lightBg, padding: "15px 0" }} {...rest}>
      <Container>
        <div className="d-flex align-items-center">
          <Heading
            as="span"
            css={{
              color: '#009688',
              fontSize: 25,
              fontWeight: 800,
              'span': {
                background: '#ffc41f',
                textTransform: 'uppercase',
                padding: '0px 6px',
                color: '#2f2f2f',
                borderRadius: 22,
                fontSize: 9,
                fontWeight: 700,
                transform: 'translate(-23px, 12px)',
                display: 'inline-block'
              }
            }}
          >
            <AnchorButton onClick={() => router.push(`/`)}>Halyk<span>Cyber</span></AnchorButton>
          </Heading>
          <nav
            css={{
              marginLeft: 30
            }}
          >
            <NavigationItem onClick={() => router.push(`/`)}>Проверить сайт</NavigationItem>
            {isAuthenticated && <NavigationItem onClick={() => router.push('/spam')}>Спам-атака</NavigationItem>}
          </nav>
          <nav
            css={{
              marginLeft: "auto"
            }}>
            {isAuthenticated ? (
              <Fragment>
                {user ? (
                  <Dropdown
                    offset={[0, 10]}
                    trigger='click'
                    placement="bottom-end"
                    animation='shift-away'
                    maxWidth={500}
                    button={
                      <button
                        className="d-block"
                        css={{
                          fontSize: 13,
                          fontFamily: "inherit",
                          background: "transparent",
                          border: "none",
                          cursor: "pointer",
                          outline: "none",
                          width: 160,
                          transition: "color 0.2s",
                          "&:hover": {
                            color: theme.accentBlue,
                          }
                        }}
                      >
                        <div className="d-flex align-items-center justify-content-end">
                          <Avatar
                            url={getAvatar(user.avatar)}
                            css={{
                              display: "inline-block",
                              width: 35,
                              marginRight: 15
                            }}
                          />
                          <div>
                            {user.name}
                            <FontAwesomeIcon className="ml-2" icon={faAngleDown} />
                          </div>
                        </div>
                      </button>
                    }
                  >
                    <Card
                      css={{
                        width: 160,
                        boxShadow: theme.blockShadow,
                        padding: "15px 25px"
                      }}
                    >
                      <DropdownItem onClick={handleLogout}>Выйти</DropdownItem>
                    </Card>
                  </Dropdown>
                  ) : null}
              </Fragment>
            ) : (
              <Fragment>
                <NavigationItem onClick={() => router.push(`/login`)}>Войти</NavigationItem>
              </Fragment>
            )}
          </nav>
        </div>
      </Container>
    </header>
  );
}
