import Head from 'next/head';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC, useState } from 'react';
import locales from '../../../core/locales';
import { useRouter } from 'next/router';
import { Container } from '../../atoms/Container';
import { Heading } from '../../atoms/Heading';
import { Button } from '../../atoms/Button';
import validator from 'validator';
import { controller, notifyError } from '../../../core';
import { AnchorButton } from '../../atoms/AnchorButton';
import { Input } from '../../atoms/Input';
import { useTheme } from '@emotion/react';
import { User } from '../../types/User';

const Login: FC<Props> = ({language, accessToken}: Props) => {
  const router = useRouter();
  const theme = useTheme();

  const [login, setLogin] = useState<string>();
  const [password, setPassword] = useState<string>();

  if (accessToken?.length) {
    router.push('/');
  }

  const handleSubmit = () => {
    if (!login.length) {
      notifyError('Неправильная электронная почта');
      return null;
    }
    if (!password.length) {
      notifyError('Заполните пароль');
      return null;
    }
    controller.post(`/api/token`, { login, password })
      .then(res => {
        if (res.data.token) {
          localStorage.setItem('access_token', res.data.token);
          const user: User = {
            name: login
          }
          localStorage.setItem('user', JSON.stringify(user))
          localStorage.setItem('access_token_expire', "10");
          location.reload();
        }
      })
      .catch(() => {
        notifyError("Неправильное имя пользователя или пароль");
      });
  };


  return (
    <Container
      isForm
      css={{
        paddingTop: 200
      }}
    >
      <Head>
        <title>Вход</title>
      </Head>
      <Heading
        as="h1"
        css={{
          textAlign: 'center',
          fontSize: 22,
          marginBottom: 20
        }}
      >Вход</Heading>

      <Input
        type="text"
        label="Имя пользователя"
        placeholder="Введите имя пользователя"
        onChange={(event) => setLogin(event.currentTarget.value)}
      />
      <Input
        type="password"
        label="Пароль"
        placeholder="Введите пароль"
        onChange={(event) => setPassword(event.currentTarget.value)}
      />

      <div className="d-flex align-items-center mt-3 justify-content-center">
        <Button
          css={{
            padding: "10px 45px",
            marginRight: 25,
            display: "inline-block",
            width: "auto",
            fontSize: 14,
            textTransform: "capitalize",
          }}
          onClick={handleSubmit}
        >Войти</Button>
      </div>


    </Container>
  );
};

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  return {props: {language: locales[language]}};
};

export default Login;
