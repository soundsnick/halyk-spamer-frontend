import Head from 'next/head';
import { GetServerSideProps } from 'next';
import React, { FC, useState } from 'react';
import locales from '../../../core/locales';
import { useRouter } from 'next/router';
import { css, useTheme } from '@emotion/react';
import { Container } from '../../atoms/Container';
import { controller, notifyError, notifySuccess, Regex } from '../../../core';
import { Title } from '../../atoms/Title';
import { Input } from '../../atoms/Input';
import { Button } from '../../atoms/Button';
import Select from 'react-select'
import { Textarea } from '../../atoms/Textarea';
import classNames from 'classnames';
import { Notification } from '../../molecules/Notification';
import { Card } from '../../molecules/Card';
import { Result } from '../../molecules/Result';
import { Paragraph } from '../../atoms/Paragraph';

const Index: FC = () => {
  const theme = useTheme();
  const router = useRouter();
  const [text, setText] = useState<string>("");
  const [isText, setIsText] = useState<boolean>(false);
  const [result, setResult] = useState<string>();
  const handleCheck = () => {
    if (isText) {
      if (text.length > 20) {
        controller.post(`/classify/sms`, { url: text })
          .then(res => {
            if(res.data) {
              // defacement/phishing/ok
              setResult(res.data.result);
            }
          })
          .catch(() => {
            notifyError("Сервис недоступен. Попробуйте позже");
          })
      } else {
        notifyError("Ошибка! Текст должен быть больше 50 символов");
      }
    }
    else {
      if (text.match(Regex.URL)) {
        notifySuccess("Успешно! Мы сейчас все проверим...");
        controller.post(`/classify/url`, { url: text })
          .then(res => {
            if(res.data) {
              // spam/ok
              setResult(res.data.result);
            }
          })
          .catch(() => {
            notifyError("Сервис недоступен. Попробуйте позже");
          })
      } else {
        notifyError("Ошибка! Введите корректный URL-адрес сайта");
      }
    }
  }

  const options = [
    { value: true, label: 'Текст' },
    { value: false, label: 'Сайт' },
  ];

  return (
    <Container
      css={{
        paddingTop: 100
      }}
      mini
    >
      {result && text.length ? (
        <Result css={{ marginBottom: 10 }} status={result} />
      ) : null}
      <Card css={{
        padding: 50
      }}>
        <Title
          className="mt-3 d-flex align-items-center justify-content-center"
          as="h1"
          css={css`
            color: #18655b;
            text-transform: uppercase;
            font-size: 28px;
            font-weight: 700;
          `}
        >
          Проверить на мошенничество
        </Title>
        <Paragraph css={{
          color: 'grey',
          fontSize: 13,
          maxWidth: 400,
          margin: 'auto',
          textAlign: 'center',
          marginTop: 7,
          marginBottom: 30
        }}>
          Модуль позволит вам проверить сайт или текст на наличие признаков мошенничества
        </Paragraph>
        <div>
          <Select
            css={{
              background: 'transparent',
              cursor: 'pointer',
              fontSize: 13,
              marginBottom: 10
            }}
            isSearchable={false}
            options={options}
            defaultValue={options[1]}
            onChange={(item) => {
              setIsText(item.value);
              setText("");
            }}
          />
        </div>

        <div className={classNames({ "d-flex": !isText }, "align-items-center")}>
          <div className="col px-0">
            {isText ? (
              <>
                {text.length < 20 ? <span css={{
                  color: 'red',
                  fontSize: 11,
                  marginBottom: 5,
                  display: 'block'
                }}>Еще {20-text.length} символов</span> : null}
                <Textarea
                  placeholder="Введите текст..."
                  onChange={e => {
                    setText(e.currentTarget?.value);
                    setResult(null);
                  }}
                />
              </>
              ) :
              (
                <Input
                  placeholder="Введите URL сайта..."
                  onChange={e => {
                    setText(e.currentTarget?.value);
                    setResult(null);
                  }}
                />
              )
            }

          </div>
          <Button
            css={{
              marginBottom: 10,
              marginLeft: 10
            }}
            onClick={handleCheck}
          >Проверить</Button>
        </div>
      </Card>
    </Container>
  );
}

export const getServerSideProps: GetServerSideProps = async ctx => {

  return { props: {} }
}

export default Index;
