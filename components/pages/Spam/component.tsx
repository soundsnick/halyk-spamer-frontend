import Head from 'next/head';
import { GetServerSideProps } from 'next';
import React, { FC, useState } from 'react';
import { useRouter } from 'next/router';
import { css, useTheme } from '@emotion/react';
import { Container } from '../../atoms/Container';
import { Title } from '../../atoms/Title';
import { Input } from '../../atoms/Input';
import { Button } from '../../atoms/Button';
import classNames from 'classnames';
import { Card } from '../../molecules/Card';
import { Paragraph } from '../../atoms/Paragraph';
import { controller, notifyError, notifySuccess } from '../../../core';

const Spam: FC = () => {
  const theme = useTheme();
  const router = useRouter();
  const [text, setText] = useState<string>("");
  const [interval, setInterval] = useState<number>(0);
  const [numStreams, setNumStreams] = useState<number>(0);
  const [isText, setIsText] = useState<boolean>(false);
  const handleCheck = () => {
    controller.post(`/api/spam`, {
      interval,
      num_streams: numStreams,
      url: text
    })
      .then(res => {
        notifySuccess("Спам-атака на мошеннический сайт запущена");
      })
      .catch(() => {
        notifyError("Сервис недоступен. Попробуйте позже")
      });
  }

  return (
    <Container
      css={{
        paddingTop: 100
      }}
      mini
    >
      <Card css={{
        padding: 50
      }}>
        <Title
          className="mt-3 d-flex align-items-center justify-content-center"
          as="h1"
          css={css`
          color: #18655b;
          text-transform: uppercase;
          font-size: 28px;
          font-weight: 700;
        `}
        >
          Спам-атака мошеннического сайта
        </Title>

        <Paragraph css={{
          color: 'grey',
          fontSize: 13,
          maxWidth: 400,
          margin: 'auto',
          textAlign: 'center',
          marginTop: 7,
          marginBottom: 30
        }}>
          Модуль позволит вам атаковать сайт мошенника
        </Paragraph>

        <div className={classNames("align-items-center")}>
          <div className="col px-0">
            <div className="d-flex align-items-center" css={{ gridGap: 10 }}>
              <div css={{ flex: '1' }}>
                <Input
                  type="number"
                  min={0}
                  placeholder="Интервал (в секундах)"
                  onChange={e => setInterval(Number(e.currentTarget?.value))}
                />
              </div>
              <div css={{ flex: '1' }}>
                <Input
                  type="number"
                  min={0}
                  placeholder="Количество потоков"
                  onChange={e => setNumStreams(Number(e.currentTarget?.value))}
                />
              </div>
            </div>
            <Input
              placeholder="Введите URL сайта..."
              onChange={e => setText(e.currentTarget?.value)}
            />
          </div>
          <Button
            css={{
              marginBottom: 10,
            }}
            isError
            onClick={handleCheck}
          >Начать атаку</Button>
        </div>
      </Card>
    </Container>
  );
}

export const getServerSideProps: GetServerSideProps = async ctx => {

  return { props: {} }
}

export default Spam;
