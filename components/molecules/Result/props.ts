import { CardProps } from '../Card';

export type Props = CardProps & {
  readonly status: string;
};
