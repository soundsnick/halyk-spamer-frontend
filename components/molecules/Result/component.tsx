import { Props } from './props';
import React, { FC } from 'react';
import { Card } from '../Card';
import { Heading } from '../../atoms/Heading';
import { Paragraph } from '../../atoms/Paragraph';
import { css } from '@emotion/react';

const getTitle = (status) => {
  switch (status) {
    case 'spam':
      return 'Обнаружены признаки Спама';
    case 'phishing':
      return 'Обнаружены признаки Фишинга';
    case 'defacement':
      return 'Обнаружены признаки подделки сайта';
    default:
      return 'Мошенничество не было обнаружено'
  }
}

const getDescription = (status) => {
  switch (status) {
    case 'spam':
      return ['Ненужные адресату электронные послания, рекламные письма и т. п., рассылаемые отдельными фирмами по Интернету или электронной почте.', 'https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B0%D0%BC'];
    case 'phishing':
      return ['Фишинг (phishing, от fishing — рыбная ловля, выуживание) — вид интернет-мошенничества, целью которого является получение идентификационных данных пользователей (логины и пароли к банковским картам, учетным записям).', 'https://ru.wikipedia.org/wiki/%D0%A4%D0%B8%D1%88%D0%B8%D0%BD%D0%B3'];
    case 'defacement':
      return ['Дефейс – тип хакерской атаки, при которой страница web-сайта заменяется другой, чаще всего содержащей рекламу, угрозы или вызывающие предупреждения, страницей. В дополнение к этому, злоумышленники либо блокируют доступ к остальным сегментам сайта, либо вообще удаляют прежнее содержимое ресурса.', 'https://ru.wikipedia.org/wiki/%D0%94%D0%B5%D1%84%D0%B5%D0%B9%D1%81'];
    default:
      return ['Проверка не обнаружила признаки мошенничества']
  }
}

export const Result: FC<Props> = ({ status, children, ...rest }: Props) => {
  const isOk = status === 'ok';
  return (
    <Card
      className="animate__animated animate__fadeIn"
      css={{ background: isOk ? "#4CAF50" : "#ff564a", padding: 30 }}
      {...rest}
    >
      <Heading
        as={'h1'}
        css={{
          color: '#fff',
          textAlign: 'center',
          marginTop: 10,
          marginBottom: 0
        }}
      >{getTitle(status)}</Heading>
      <Paragraph css={{ color: '#fff', textAlign: status === 'ok' ? 'center' : 'left' }}>
        {getDescription(status)[0]}
        {getDescription(status)[1] && (
          <a
            href={getDescription(status)[1]}
            target="_blank"
            css={css`
              color: #fff358;
              font-weight: 600;
              text-decoration: underline;
              margin-left: 10px;
            `}
          >Статья на Википедии</a>
        )}
      </Paragraph>
    </Card>
  );
}
