import { User } from "./User";

export type Article = {
    id: number;
    title: string;
    content: string;
    description: string;
    cover: string;
    user_id: number;
    User: User;
    created_at: string;
    updated_at: string;
}
