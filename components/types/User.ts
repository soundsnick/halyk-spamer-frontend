export type User = {
  id?: number;
  name?: string;
  last_name?: string;
  avatar?: string;
  updated_at?: string;
  deleted_at?: string;
  created_at?: string;
}
