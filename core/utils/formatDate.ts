import moment from "moment";

 export const formatDate = (dateStr) => moment(dateStr).format("DD.MM.YYYY hh:mm");
