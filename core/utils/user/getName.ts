import { User } from '../../../components/types/User';

export const getName = (user: User) => `${user.name} ${user.last_name}`;
