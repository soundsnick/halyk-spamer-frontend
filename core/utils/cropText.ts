export const cropText = (str: string, count: number = 30) => str.length ? `${str.slice(0, count)}...` : null;
