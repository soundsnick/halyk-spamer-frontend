import axios from "axios";

export const apiBaseUrl = "http://185.251.89.129:9999";

export const controller = axios.create({
  baseURL: apiBaseUrl,
  responseType: "json"
});
